package com.estech.listadoscambiantes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PersonajeAdapter2 extends RecyclerView.Adapter<PersonajeAdapter2.PersonajeHolder> {

    private List<Personaje> personajes;
    Context context;

    public PersonajeAdapter2(List<Personaje> personajes, Context context) {
        this.personajes = personajes;
        this.context = context;
    }

    @NonNull
    @Override
    public PersonajeHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        int layout = R.layout.item_list0;
        if (viewType == 1) {
            layout = R.layout.item_list1;
        } else if (viewType == 2){
            layout = R.layout.item_list2;
        }
        View itemView = LayoutInflater.from(parent.getContext()).inflate(layout, parent, false);
        PersonajeHolder holder = new PersonajeHolder(itemView);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull PersonajeHolder holder, int position) {
        Personaje personaje = personajes.get(position);
        holder.tvName.setText(personaje.getName());
       /* holder.tvStatus.setText(personaje.getStatus());
        holder.tvSpecies.setText(personaje.getSpecies());
        holder.tvGender.setText(personaje.getGender());
        holder.tvOrigin.setText(personaje.getOrigin().getName());
        holder.tvLocalitation.setText(personaje.getLocation().getName());
*/
        holder.layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Toast.makeText(context, personaje.getName(),Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(context, PersonajeDetalle.class);
//                i.putExtra("data", personaje);
//                context.startActivity(i);
            }
        });

        Picasso.get().load(personaje.getImage()).into(holder.ivImage);
    }

    @Override
    public int getItemCount() {
        return personajes.size();
    }

    @Override
    public int getItemViewType(int position) {
        Personaje personaje = personajes.get(position);
        String genero = personaje.getGender().toLowerCase();
        if (genero.equals("male")) {
            return 1;
        } else if (genero.equals("female")) {
            return 2;
        } else {
            return 0;
        }
    }

    public void setLista(List<Personaje> lista) {
        this.personajes = lista;
        notifyDataSetChanged(); //Con esto se refresca el adapter, recargar
    }

    public void addNewList(List<Personaje> lista) {
        personajes.addAll(lista);//Combina ambos listados
        notifyItemRangeInserted(getItemCount(), personajes.size() - 1);//Notifica que se ha insertado un rango, sin necesitar modificar la lista
    }

    static class PersonajeHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvStatus, tvSpecies, tvOrigin, tvLocalitation, tvGender;
        ImageView ivImage;
        LinearLayout layout;

        public PersonajeHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.txtTitle);
           /* tvStatus = v.findViewById(R.id.txtStatus);
            tvSpecies = v.findViewById(R.id.txtSpecies);
            tvOrigin = v.findViewById(R.id.txtOrigin);
            tvLocalitation = v.findViewById(R.id.txtLastLocalitation);
            tvGender = v.findViewById(R.id.txtGender);*/
            ivImage = v.findViewById(R.id.imagePortada);
            layout = v.findViewById(R.id.layout);
        }

    }
}
