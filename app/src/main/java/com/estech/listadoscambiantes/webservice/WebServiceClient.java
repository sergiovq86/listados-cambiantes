package com.estech.listadoscambiantes.webservice;


import com.estech.listadoscambiantes.modelsEpisodio.DatosEpisodio;
import com.estech.listadoscambiantes.modelsEpisodio.Episodio;
import com.estech.listadoscambiantes.modelsLocation.LocationResponse;
import com.estech.listadoscambiantes.modelsPersonajes.Datos;
import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.estech.listadoscambiantes.modelsPersonajes.Ubicacion;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import retrofit2.http.Url;

public interface WebServiceClient {

    String BASE_URL = "https://rickandmortyapi.com/api/";

    @GET("character")
    Call<Datos> getPersonajes();

    @GET("location")
    Call<LocationResponse> getLocation();

    @GET()
    Call<Ubicacion> getUbicacion(@Url String url);

    @GET()
    Call<List<Personaje>> getPersonajeById(@Url String url);

    @GET("character")
    Call<Datos> getPersonajesByName(@Query("name") String name);

    @GET("character")
    Call<Datos> getNextPage(@Query("page") String page);

    @GET("episode")
    Call<DatosEpisodio> getEpisodios();

    @GET("episode")
    Call<DatosEpisodio> getNextPageEpisode(@Query("page") String page);

    @GET
    Call<Episodio> getEpisodioDetalle(@Url String url);

}
