package com.estech.listadoscambiantes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estech.listadoscambiantes.modelsPersonajes.Datos;
import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.estech.listadoscambiantes.webservice.WebServiceClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Listado1 extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PersonajeAdapter adapter;
    private List<Personaje> personajeList;
    private ProgressBar barraProgeso;

    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barraProgeso = findViewById(R.id.progressBar);
        barraProgeso.setVisibility(View.VISIBLE);

        setupView();
        lanzarPeticion();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.recyclerview);
        personajeList = new ArrayList<>();
        adapter = new PersonajeAdapter(personajeList, this);
        layoutManager = new GridLayoutManager(this, 2);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (position % 3 == 0) {
                    return 2;
                } else
                    return 1;
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void lanzarPeticion() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(WebServiceClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();
        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<Datos> peticion = client.getPersonajes();
        peticion.enqueue(new Callback<Datos>() {
            @Override
            public void onResponse(Call<Datos> call, Response<Datos> response) {
                if (response.isSuccessful()) {
                    int code = response.code();
                    barraProgeso.setVisibility(View.GONE);
                    Datos misDatos = response.body();
                    List<Personaje> listado = misDatos.getResults();
                    adapter.setLista(listado);
                } else {
                    int code = response.code();
                    if (code == 404) {

                    }
                }
            }

            @Override
            public void onFailure(Call<Datos> call, Throwable t) {
                barraProgeso.setVisibility(View.GONE);
                Log.d("RETROFIT", "Error: " + t.getMessage());
            }
        });
    }
}