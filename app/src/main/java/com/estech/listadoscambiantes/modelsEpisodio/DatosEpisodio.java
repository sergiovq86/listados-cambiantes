package com.estech.listadoscambiantes.modelsEpisodio;


import com.estech.listadoscambiantes.modelsPersonajes.Info;

import java.util.List;

public class DatosEpisodio {

    private Info info;
    private List<Episodio> results;

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Episodio> getResults() {
        return results;
    }

    public void setResults(List<Episodio> results) {
        this.results = results;
    }
}
