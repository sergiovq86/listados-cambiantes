package com.estech.listadoscambiantes.modelsEpisodio;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

public class Episodio implements Parcelable {

    private int id;
    private String name, air_date, episode, url, created;
    private ArrayList<String> characters;

    protected Episodio(Parcel in) {
        id = in.readInt();
        name = in.readString();
        air_date = in.readString();
        episode = in.readString();
        url = in.readString();
        created = in.readString();
        characters = in.createStringArrayList();
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(name);
        dest.writeString(air_date);
        dest.writeString(episode);
        dest.writeString(url);
        dest.writeString(created);
        dest.writeStringList(characters);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public static final Creator<Episodio> CREATOR = new Creator<Episodio>() {
        @Override
        public Episodio createFromParcel(Parcel in) {
            return new Episodio(in);
        }

        @Override
        public Episodio[] newArray(int size) {
            return new Episodio[size];
        }
    };

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAir_date() {
        return air_date;
    }

    public void setAir_date(String air_date) {
        this.air_date = air_date;
    }

    public String getEpisode() {
        return episode;
    }

    public void setEpisode(String episode) {
        this.episode = episode;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getCreated() {
        return created;
    }

    public void setCreated(String created) {
        this.created = created;
    }

    public ArrayList<String> getCharacters() {
        return characters;
    }

    public void setCharacters(ArrayList<String> characters) {
        this.characters = characters;
    }
}
