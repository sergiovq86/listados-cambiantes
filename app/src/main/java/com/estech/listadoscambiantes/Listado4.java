package com.estech.listadoscambiantes;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.estech.listadoscambiantes.modelsEpisodio.DatosEpisodio;
import com.estech.listadoscambiantes.modelsEpisodio.Episodio;
import com.estech.listadoscambiantes.modelsLocation.LocationResponse;
import com.estech.listadoscambiantes.modelsLocation.Lugar;
import com.estech.listadoscambiantes.modelsPersonajes.Datos;
import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.estech.listadoscambiantes.webservice.WebServiceClient;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class Listado4 extends AppCompatActivity {

    private RecyclerView recyclerView;
    private PersonajeAdapter4 adapter;
    private List<Personaje> personajeList;
    private List<Lugar> lugaresList;
    private List<Episodio> episodioList;
    private List<Object> listaDefinitiva;

    private ProgressBar barraProgeso;

    private Retrofit retrofit;
    private HttpLoggingInterceptor loggingInterceptor;
    private OkHttpClient.Builder httpClientBuilder;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        barraProgeso = findViewById(R.id.progressBar);
        barraProgeso.setVisibility(View.VISIBLE);

        setupView();
        lanzarPeticionPersonajes();
    }

    private void setupView() {
        recyclerView = findViewById(R.id.recyclerview);
        personajeList = new ArrayList<>();
        adapter = new PersonajeAdapter4(this);
        layoutManager = new GridLayoutManager(this, 3);
        layoutManager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                Object object = listaDefinitiva.get(position);
                if (object instanceof Personaje) {
                    return 1;
                } else {
                    return 3;
                }
            }
        });
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private void lanzarPeticionPersonajes() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(WebServiceClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();
        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<Datos> peticion = client.getPersonajes();
        peticion.enqueue(new Callback<Datos>() {
            @Override
            public void onResponse(Call<Datos> call, Response<Datos> response) {
                if (response.isSuccessful()) {
                    int code = response.code();
                    Datos misDatos = response.body();
                    personajeList = misDatos.getResults();
                    lanzarPeticionLugares();
                } else {
                    barraProgeso.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<Datos> call, Throwable t) {
                barraProgeso.setVisibility(View.GONE);
                Log.d("RETROFIT", "Error: " + t.getMessage());
            }
        });
    }

    private void lanzarPeticionLugares() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(WebServiceClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();
        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<LocationResponse> peticion = client.getLocation();
        peticion.enqueue(new Callback<LocationResponse>() {
            @Override
            public void onResponse(Call<LocationResponse> call, Response<LocationResponse> response) {
                if (response.isSuccessful()) {
                    int code = response.code();
                    LocationResponse misDatos = response.body();
                    lugaresList = misDatos.getResults();
                    lanzarPeticionEpisodios();
                } else {
                    barraProgeso.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<LocationResponse> call, Throwable t) {
                barraProgeso.setVisibility(View.GONE);
                Log.d("RETROFIT", "Error: " + t.getMessage());
            }
        });
    }

    private void lanzarPeticionEpisodios() {
        loggingInterceptor = new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClientBuilder = new OkHttpClient.Builder().addInterceptor(loggingInterceptor);

        retrofit = new Retrofit.Builder()
                .baseUrl(WebServiceClient.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .client(httpClientBuilder.build())
                .build();
        WebServiceClient client = retrofit.create(WebServiceClient.class);

        Call<DatosEpisodio> peticion = client.getEpisodios();
        peticion.enqueue(new Callback<DatosEpisodio>() {
            @Override
            public void onResponse(Call<DatosEpisodio> call, Response<DatosEpisodio> response) {
                if (response.isSuccessful()) {
                    int code = response.code();
                    DatosEpisodio misDatos = response.body();
                    episodioList = misDatos.getResults();
                    crearListadoDefinitivo();
                    barraProgeso.setVisibility(View.GONE);
                } else {
                    barraProgeso.setVisibility(View.GONE);
                }
            }

            @Override
            public void onFailure(Call<DatosEpisodio> call, Throwable t) {
                barraProgeso.setVisibility(View.GONE);
                Log.d("RETROFIT", "Error: " + t.getMessage());
            }
        });
    }

    private void crearListadoDefinitivo() {
        listaDefinitiva = new ArrayList<>();
        listaDefinitiva.add("Personajes");
        listaDefinitiva.addAll(personajeList);
        listaDefinitiva.add("Episodios");
        listaDefinitiva.addAll(episodioList);
        listaDefinitiva.add("Lugares");
        listaDefinitiva.addAll(lugaresList);

        adapter.setLista(listaDefinitiva);
    }
}