package com.estech.listadoscambiantes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.squareup.picasso.Picasso;

import java.util.List;

public class PersonajeAdapter3 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Personaje> personajes;
    Context context;

    public PersonajeAdapter3(List<Personaje> personajes, Context context) {
        this.personajes = personajes;
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == 0) {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mod1, parent, false);
            PersonajeHolder holder = new PersonajeHolder(itemView);
            return holder;
        } else {
            View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_list_mod2, parent, false);
            ModHolder holder = new ModHolder(itemView);
            return holder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        Personaje personaje = personajes.get(position);
        if (viewType == 0) {
            PersonajeHolder pHolder = (PersonajeHolder) holder;
            pHolder.tvName.setText(personaje.getName());
            Picasso.get().load(personaje.getImage()).into(pHolder.ivImage);
        } else {
            ModHolder mHolder = (ModHolder) holder;
            mHolder.tvName.setText(personaje.getSpecies());
            Picasso.get().load(personaje.getImage()).into(mHolder.ivImage);
        }
    }

    @Override
    public int getItemCount() {
        return personajes.size();
    }

    @Override
    public int getItemViewType(int position) {

        if (position % 2 == 0) {
            return 0;
        } else
            return 1;
    }

    public void setLista(List<Personaje> lista) {
        this.personajes = lista;
        notifyDataSetChanged(); //Con esto se refresca el adapter, recargar
    }

    public void addNewList(List<Personaje> lista) {
        personajes.addAll(lista);//Combina ambos listados
        notifyItemRangeInserted(getItemCount(), personajes.size() - 1);//Notifica que se ha insertado un rango, sin necesitar modificar la lista
    }

    static class PersonajeHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvStatus, tvSpecies, tvOrigin, tvLocalitation, tvGender;
        ImageView ivImage;
        LinearLayout layout;

        public PersonajeHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.txtTitle);
           /* tvStatus = v.findViewById(R.id.txtStatus);
            tvSpecies = v.findViewById(R.id.txtSpecies);
            tvOrigin = v.findViewById(R.id.txtOrigin);
            tvLocalitation = v.findViewById(R.id.txtLastLocalitation);
            tvGender = v.findViewById(R.id.txtGender);*/
            ivImage = v.findViewById(R.id.imagePortada);
            layout = v.findViewById(R.id.layout);
        }

    }

    static class ModHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvStatus, tvSpecies, tvOrigin, tvLocalitation, tvGender;
        ImageView ivImage;
        LinearLayout layout;

        public ModHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.txtTitle);
           /* tvStatus = v.findViewById(R.id.txtStatus);
            tvSpecies = v.findViewById(R.id.txtSpecies);
            tvOrigin = v.findViewById(R.id.txtOrigin);
            tvLocalitation = v.findViewById(R.id.txtLastLocalitation);
            tvGender = v.findViewById(R.id.txtGender);*/
            ivImage = v.findViewById(R.id.imagePortada);
            layout = v.findViewById(R.id.layout);
        }

    }
}
