package com.estech.listadoscambiantes.modelsPersonajes;

import android.os.Parcel;
import android.os.Parcelable;

public class Localitation implements Parcelable {

    private String name,url;

    protected Localitation(Parcel in) {
        name = in.readString();
        url = in.readString();
    }

    public static final Creator<Localitation> CREATOR = new Creator<Localitation>() {
        @Override
        public Localitation createFromParcel(Parcel in) {
            return new Localitation(in);
        }

        @Override
        public Localitation[] newArray(int size) {
            return new Localitation[size];
        }
    };

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(name);
        dest.writeString(url);
    }
}
