package com.estech.listadoscambiantes.modelsPersonajes;

import java.util.List;

public class Datos {

    private Info info;
    private List<Personaje> results; //Tiene que llamarse igual que en el JSON

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Personaje> getResults() {
        return results;
    }

    public void setResults(List<Personaje> results) {
        this.results = results;
    }
}
