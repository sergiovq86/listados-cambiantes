package com.estech.listadoscambiantes;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.estech.listadoscambiantes.modelsEpisodio.Episodio;
import com.estech.listadoscambiantes.modelsLocation.Lugar;
import com.estech.listadoscambiantes.modelsPersonajes.Personaje;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

public class PersonajeAdapter4 extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<Object> objetos;
    Context context;

    public PersonajeAdapter4(Context context) {
        objetos = new ArrayList<>();
        this.context = context;
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        switch (viewType) {
            default:
                View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.titulo_item, parent, false);
                TitleHolder tHolder = new TitleHolder(itemView);
                return tHolder;

            case 2:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.personajes_item, parent, false);
                PersonajeHolder holder = new PersonajeHolder(itemView);
                return holder;

            case 3:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.episodioitem, parent, false);
                EpisodioHolder eHolder = new EpisodioHolder(itemView);
                return eHolder;

            case 4:
                itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.lugares_item, parent, false);
                LugarHolder lHolder = new LugarHolder(itemView);
                return lHolder;
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        int viewType = getItemViewType(position);
        Object objeto = objetos.get(position);

        switch (viewType) {
            default:
                String titulo = (String) objeto;
                TitleHolder tHolder = (TitleHolder) holder;
                tHolder.tvTitle.setText(titulo);
                break;

            case 2:
                Personaje personaje = (Personaje) objeto;
                PersonajeHolder pHolder = (PersonajeHolder) holder;
                pHolder.tvName.setText(personaje.getName());
                Picasso.get().load(personaje.getImage()).into(pHolder.ivImage);
                break;

            case 3:
                Episodio episodio = (Episodio) objeto;
                EpisodioHolder eHolder = (EpisodioHolder) holder;
                eHolder.tvEpisode.setText(episodio.getEpisode());
                eHolder.tvAirDate.setText(episodio.getAir_date());
                eHolder.tvName.setText(episodio.getName());
                break;

            case 4:
                Lugar lugar = (Lugar) objeto;
                LugarHolder lHolder = (LugarHolder) holder;
                lHolder.tvDimension.setText(lugar.getDimension());
                lHolder.tvType.setText(lugar.getType());
                lHolder.tvName.setText(lugar.getName());
                break;
        }

    }

    @Override
    public int getItemCount() {
        return objetos.size();
    }

    @Override
    public int getItemViewType(int position) {
        Object object = objetos.get(position);
        if (object instanceof Personaje) {
            return 2;
        } else if (object instanceof Episodio) {
            return 3;
        } else if (object instanceof Lugar) {
            return 4;
        } else {
            return 1;
        }
    }

    public void setLista(List<Object> lista) {
        this.objetos = lista;
        notifyDataSetChanged(); //Con esto se refresca el adapter, recargar
    }

    public void addNewList(List<Personaje> lista) {
        objetos.addAll(lista);//Combina ambos listados
        notifyItemRangeInserted(getItemCount(), objetos.size() - 1);//Notifica que se ha insertado un rango, sin necesitar modificar la lista
    }

    static class PersonajeHolder extends RecyclerView.ViewHolder {

        TextView tvName;
        ImageView ivImage;
        LinearLayout layout;

        public PersonajeHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.txtTitle);
            ivImage = v.findViewById(R.id.imagePortada);
            layout = v.findViewById(R.id.layout);
        }

    }

    static class LugarHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvType, tvDimension;

        public LugarHolder(@NonNull View v) {
            super(v);
            tvName = v.findViewById(R.id.txtName);
            tvType = v.findViewById(R.id.txtType);
            tvDimension = v.findViewById(R.id.txtDimension);
        }

    }

    static class TitleHolder extends RecyclerView.ViewHolder {
        TextView tvTitle;

        public TitleHolder(@NonNull View itemView) {
            super(itemView);
            tvTitle = itemView.findViewById(R.id.txtName);
        }
    }

    static class EpisodioHolder extends RecyclerView.ViewHolder {

        TextView tvName, tvEpisode, tvAirDate;

        public EpisodioHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.txtName);
            tvAirDate = itemView.findViewById(R.id.txtAirDate);
            tvEpisode = itemView.findViewById(R.id.txtEpisode);
        }
    }
}
