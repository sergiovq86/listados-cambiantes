
package com.estech.listadoscambiantes.modelsLocation;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class LocationResponse {

    @SerializedName("info")
    @Expose
    private Info info;
    @SerializedName("results")
    @Expose
    private List<Lugar> results = null;

    /**
     * No args constructor for use in serialization
     * 
     */
    public LocationResponse() {
    }

    /**
     * 
     * @param results
     * @param info
     */
    public LocationResponse(Info info, List<Lugar> results) {
        super();
        this.info = info;
        this.results = results;
    }

    public Info getInfo() {
        return info;
    }

    public void setInfo(Info info) {
        this.info = info;
    }

    public List<Lugar> getResults() {
        return results;
    }

    public void setResults(List<Lugar> results) {
        this.results = results;
    }

}
